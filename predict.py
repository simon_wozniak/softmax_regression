# --------------------------------------------------------------------------
# ------------  Metody Systemowe i Decyzyjne w Informatyce  ----------------
# --------------------------------------------------------------------------
#  Zadanie 4: Zadanie zaliczeniowe
#  autorzy: A. Gonczarek, J. Kaczmar, S. Zareba
#  2017
# --------------------------------------------------------------------------

import pickle as pkl
import numpy as np
import utils
import time

DEF_ETAS = [0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 1]
DEF_LAMBDAS = [0, 0.00001, 0.0001, 0.001, 0.01, 0.1]
DEF_ITER = [100]


def my_extraction(x_images):
    features_of_images = []
    for image in x_images:
        image_features = []
        image = np.reshape(image, (56, 56))
        for i in range(8):
            for k in range(8):
                image_features.append(np.mean(image[i * 7:i * 7 + 7, k * 7:k * 7 + 7]))
        features_of_images.append(image_features)
    print(np.shape(features_of_images))
    return np.array(features_of_images)


def softmax(X, Thetas):
    L = np.exp(X @ Thetas.T)
    P = L / np.sum(L, axis=1, keepdims=True)
    return P


def softmax_grad(X, Y, Thetas):
    P = softmax(X, Thetas)
    N = np.shape(X)[0]
    return -((Y - P).T @ X) / N


def softmax_cost(X, Y, Thetas):
    A = X @ Thetas.T
    N = np.shape(X)[0]
    res = -(np.sum(Y * A - np.log(np.sum(np.exp(A), axis=1))[:, np.newaxis])) / N
    print(res)
    return res


def regularized_softmax_grad(X, Y, Thetas, reg_lambda):
    grad = softmax_grad(X, Y, Thetas)
    reg = Thetas * reg_lambda
    return grad + reg


def stochastic_gradient_descent(grad_fun, x_train, y_train, w0, epochs, eta, mini_batch):
    w = w0
    batches_number = len(x_train) // mini_batch
    x_batches = np.vsplit(x_train, batches_number)
    y_batches = np.vsplit(y_train, batches_number)
    for k in range(epochs):
        for m in range(batches_number):
            grad = grad_fun(x_batches[m], y_batches[m], w)
            delta_w = -grad
            w += eta * delta_w
    return w


def predict(x):
    """
    Funkcja pobiera macierz przykladow zapisanych w macierzy X o wymiarach NxD i zwraca wektor y o wymiarach Nx1,
    gdzie kazdy element jest z zakresu {0, ..., 35} i oznacza znak rozpoznany na danym przykladzie.
    :param x: macierz o wymiarach NxD
    :return: wektor o wymiarach Nx1
    """
    pass


def feature_extraction(x_images):
    features_of_images = []
    for image in x_images:
        image_features = []
        image = np.reshape(image, (56, 56))
        for i in range(4):
            for k in range(4):
                image_features.append(np.mean(image[i*14:i*14+14, k*14:k*14+14]))
        features_of_images.append(image_features)
    return np.array(features_of_images)


def hog_extraction(x_images):
    X = []
    for i, image in enumerate(x_images):
        X.append(np.squeeze(utils.hog(np.reshape(image, (56, 56)))))
        print(i)
    X = np.array(X)
    return X


def make_prediction(P):
    return np.argmax(P, axis=1)


def one_hot(y, k):
    ONE_HOT = np.zeros((np.shape(y)[0], k))
    for i, e in enumerate(y):
        ONE_HOT[i, e] = 1
    return ONE_HOT


def accuracy(y_pred, y_val):
    return np.sum(y_pred == y_val) / np.shape(y_pred)[0]


def model_selection(X, x_val, Y, y_val):
    best_lambda = 0
    best_eta = 0
    best_iter = 0
    best_accuracy = 0
    k = 36
    M = np.shape(X)[1]

    for l in DEF_LAMBDAS:
        def grad(X, Y, Thetas):
            return regularized_softmax_grad(X, Y, Thetas, l)
        for e in DEF_ETAS:
            for i in DEF_ITER:
                print(f"Lambda {l}, eta: {e}, iter: {i}")
                initial_thetas = np.zeros((k, M))
                thetas = stochastic_gradient_descent(grad, X, Y, initial_thetas, i, e, 50)
                P = softmax(x_val, thetas)
                preds = make_prediction(P)[:, np.newaxis]
                acc = accuracy(preds, y_val)
                if acc > best_accuracy:
                    best_lambda = l
                    best_eta = e
                    best_iter = i
                    best_accuracy = acc
                print(f"acc: {acc}")
    print(f"Best lambda: {best_lambda}, best eta: {best_eta}, best iter: {best_iter}, best accuracy: {best_accuracy}")

if __name__ == '__main__':
    with open('train.pkl', 'rb') as a:
        validation = pkl.load(a)

        # x_train = validation[0][:18000]
        # x_val = validation[0][18000:]
        # y_train = validation[1][:18000]
        # y_val = validation[1][18000:]

        x_val = validation[0][-2500:]
        y_val = validation[1][-2500:]

    start_time = time.time()
    with open('thetas.pkl', 'rb') as thetas_file:
        thetas = pkl.load(thetas_file)

    X_val = hog_extraction(x_val)
    P = softmax(X_val, thetas)
    preds = make_prediction(P)[:, np.newaxis]
    elapsed_time = time.time() - start_time
    print(f"time: {elapsed_time}")
    print(accuracy(preds, y_val))

    # k = 36
    # Y_one_hot = one_hot(y_train, k)
    # X = hog_extraction(x_train)
    # X_val = hog_extraction(x_val)
    # # X = my_extraction(x_train)
    # # X_val = my_extraction(x_val)
    # M = np.shape(X)[1]
    # initial_thetas = np.zeros((k, M))
    # model_selection(X, X_val, Y_one_hot, y_val)
    #
    # def grad(X, Y, Thetas):
    #     return regularized_softmax_grad(X, Y, Thetas, 0)
    #
    # thetas = stochastic_gradient_descent(grad, X, Y_one_hot, initial_thetas, 100, 0.3, 50)
    # print(np.shape(thetas))
    # P = softmax(X_val, thetas)
    # preds = make_prediction(P)[:, np.newaxis]
    # print(accuracy(preds, y_val))
    #
    #
    # with open('thetas.pkl', 'wb') as b:
    #     pkl.dump(thetas, b, protocol=pkl.HIGHEST_PROTOCOL)